import React from 'react';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from './src/views/HomeScreen'
import ScanScreen from './src/views/ScanScreen'
import DetailsScreen from './src/views/DetailsScreen'
import FullImage from './src/views/FullImage'
import {AsyncStorage} from "react-native";


function HomeStack() {
    return (
        <Stack.Navigator>
          <Stack.Screen name="Ma liste" component={HomeScreen} />
          <Stack.Screen name="Details" component={DetailsScreen} options={({ route }) => ({ title: route.params.product_name })} />
          <Stack.Screen name="FullImage" component={FullImage} />
        </Stack.Navigator>
    );
  }


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
  
function AppNavigator() {
    return (
      <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen  name="Ma liste" component={HomeStack} />
          <Tab.Screen name="Scanner un produit" component={ScanScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }



export default class AppContainer extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            isReady: false,
        };
    }

    async _loadAssetsAsync() {

        await Font.loadAsync({
            'linotype_brewery_bold': require('./assets/fonts/linotype_brewery_bold.ttf'),
            'SFProDisplay-Bold': require('./assets/fonts/SF-Pro-Display-Black.otf'),
            'SFProDisplay-Regular': require('./assets/fonts/SF-Pro-Display-Regular.otf')
        })
    }


        render() {
            if (!this.state.isReady) {
              return (
                <AppLoading
                  startAsync={this._loadAssetsAsync}
                  onFinish={() => this.setState({ isReady: true })}
                  onError={console.warn}
                />
              );
            }
        
            return (
                <AppNavigator />
            );
          }
    

}
