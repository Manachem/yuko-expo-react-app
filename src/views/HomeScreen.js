import React from 'react';
import { Text, View, ActivityIndicator, FlatList, StyleSheet, SafeAreaView, Button, Image,AsyncStorage } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

class ListItem extends React.Component{
    _onPress(item){
        this.props.navigation.navigate('Details',
        {
            product_name: item.product_name,
            item: item,
        })

    }
    
    render() {
        return (
            <View style={styles.lineContainer}>
                <TouchableOpacity onPress={()=> this._onPress(this.props.item)}>
                    <Text style={styles.textLino}>{this.props.item.product_name}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}


export default class HomeScreen extends React.Component {

    constructor(props){
        AsyncStorage.clear();
        super(props);
        this.state = {
            products: [],
            isLoading:true
        }
    }

    componentDidMount(){

        AsyncStorage.getItem('products').then(
            value =>
                this.setState({
                    products: JSON.parse(value),
                    isLoading: false,
                })
        );

        this.props.navigation.addListener('focus', () => {
            AsyncStorage.getItem('products').then(
                value =>
                    {
                        this.setState({
                            products: JSON.parse(value),
                            isLoading: false,
                        });
                    }
            );
        });


    }

    render() {
        if(this.state.isLoading){
            return(
              <View style={{flex: 1, padding: 20}}>
                <ActivityIndicator/>
              </View>
            )
          }


        return(
                <SafeAreaView style={{flex: 1, paddingTop:20}}>
                    <FlatList
                        data={this.state.products}
                        renderItem={({item}) => <ListItem item={item} navigation={this.props.navigation} />}
                        keyExtractor={({id}, index) => id}
                    />
                </SafeAreaView>
        );
    }
  }

  const styles = StyleSheet.create({
    lineContainer: {
      height: 40,
      padding: 10,
    },
    textLino: {
        fontFamily: 'linotype_brewery_bold'
    }
});
