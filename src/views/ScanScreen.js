import React, { useState, useEffect, Icon } from 'react';
import { Text, View, Button,AsyncStorage } from 'react-native';
import { Camera } from 'expo-camera';

const { FlashMode: CameraFlashModes, Type: CameraTypes } = Camera.Constants;

export default class ScanScreen extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            cameraFlashToggle: Camera.Constants.FlashMode.off,
            hasCameraPermission: null,
            type: Camera.Constants.Type.back,
            scanned: null,
            productScan:null,
            focused:true
        }
    }

    async componentDidMount() {
        this.checkGrantedCamera()

        this.props.navigation.addListener('focus', () => {
            this.setState({
                 focused:true
             });
        });

        this.props.navigation.addListener('blur', () => {
            this.setState({
                focused:false
            });
        });
    }

    async checkGrantedCamera(){
        const { status } = await Camera.requestPermissionsAsync();
        this.setState({ hasCameraPermission: status === 'granted' });
    }

    _flashToggle(){
        var state = this.state;
        state.cameraFlashToggle = state.cameraFlashToggle == Camera.Constants.FlashMode.off ? Camera.Constants.FlashMode.torch : Camera.Constants.FlashMode.off
        this.setState(state)
    }

    handleBarCodeScanned = ({ type, data }) => {
            fetch('https://world.openfoodfacts.org/api/v0/product/'+data+'.json')
                .then((response) => response.json())
                .then((responseJson) => {
                    if( responseJson.status === 1 ){
                        AsyncStorage.getItem('products').then(
                            value => {
                                var arr = JSON.parse(value);
                                if(arr == null){
                                    arr = [];
                                }
                                var codeCheck = true;
                                arr.forEach(function(e){
                                   if(e.code === responseJson.product.code){
                                       codeCheck = false;
                                   }
                                });
                                if ( codeCheck ){
                                    arr.push(responseJson.product);
                                    AsyncStorage.setItem('products', JSON.stringify(arr));
                                }
                            }
                        );
                        this.props.navigation.navigate('Details',
                            {
                                product_name: responseJson.product.product_name_fr,
                                item: responseJson.product,s
                            })
                    }

                })
                .catch((error) =>{
                    console.error(error);
                });
	};


    render(){

        const { hasCameraPermission,isFocused } = this.state;
        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return (
            <View>
                <Text>No access to camera</Text>
            </View>
            );
        } else {
            return (
            <View style={{ flex: 1 }}>
                {this.state.focused?
                    <Camera
                        type={this.state.type}
                        flashMode={this.state.cameraFlashToggle}
                        onBarCodeScanned={this.state.scanned ? undefined : this.handleBarCodeScanned}
                        style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'flex-end'
                        }}

                    >
                    </Camera>
                    : null}

                <Button
                    style={{ marginBottom:30, width:5, height:50, borderRadius:50}}
                    onPress={()=> this._flashToggle()}
                    title = "flash"
                />
            </View>
          );
        }
    }

  
}
