import React from 'react';
import { Text, View, StyleSheet,SafeAreaView, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

export default class DetailsScreen extends React.Component {

    render() {
          return(
            <SafeAreaView style={{flex: 1, paddingTop:20}}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('FullImage', { imageSource: this.props.route.params.item.image_url })}
                    >
                    <Image 
                        source={{uri: this.props.route.params.item.image_small_url }}
                        style={{ alignSelf: 'center', width: 200, height: 200, borderRadius: 100, marginBottom:30}}
                    />
                 </TouchableOpacity>
                <Text style={{  fontSize: 20, marginBottom:15, left:10 }} >Fiche détails :</Text>

                <Text style={{  left:10, marginBottom:60  }} > { this.props.route.params.item.categories.split(',')[0] }</Text>

                <Text style={{  fontWeight: 'bold', marginBottom:20, left:10  }} > Valeurs nutritionnelles pour 100g :</Text>
                <Text style={{  left:10, marginBottom:10, fontWeight: 'bold'  }}> Kilo/Calorie : <Text style={{ fontWeight: '300'  }}> {this.props.route.params.item.nutriments["energy-kcal"]} </Text></Text>
                <Text style={{  left:10, marginBottom:10, fontWeight: 'bold'  }}> Sel : <Text style={{ fontWeight: '300'  }}> {this.props.route.params.item.nutriments["salt"]}g </Text></Text>
                <Text style={{  left:10, marginBottom:10, fontWeight: 'bold'  }}> Protéines : <Text style={{ fontWeight: '300'  }}> {this.props.route.params.item.nutriments["proteins_100g"]}g </Text></Text>
                <Text style={{  left:10, marginBottom:10, fontWeight: 'bold'  }}> Glucides : <Text style={{ fontWeight: '300'  }}> {this.props.route.params.item.nutriments["sugars_serving"]}g </Text></Text>
            </SafeAreaView>
        );
    }
}

